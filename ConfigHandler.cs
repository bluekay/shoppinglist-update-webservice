using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ShoppingUpdate
{
    public class ConfigHandler_backup
    {
        public static Dictionary<string, string> GetResources(string path)
        {
            var rval = new Dictionary<string, string>();
            string[] files;
            try 
            { 
                files = Directory.GetFiles(path, "*.json", SearchOption.TopDirectoryOnly); 
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
                Console.WriteLine("Could not read directory!");
                return rval;
            }
            
            if (!(files.Length == 0))
            {
                foreach (string file in files)
                {
                    var filename = Path.GetFileNameWithoutExtension(file);
                    rval.Add(filename, System.IO.File.ReadAllText(@file));
                }
                
            }
            else
            {
                Console.WriteLine("Folder does not exist.");
            }
            return rval;
        }
    }
    public class ConfigHandler
    {
        public static Dictionary<string, string> GetResources(string path)
        {
            var rval = new Dictionary<string, string>();
            var filelist = GetFileList(path, "*.json", SearchOption.TopDirectoryOnly);
            if (filelist.Any())
            {
                return GetFileListContents(filelist);
            }
            else
            {
                return new Dictionary<string, string>();
            }
        }

        private static Dictionary<string, string> GetFileListContents(List<string> filelist)
        {
            Dictionary<string, string> rval = new Dictionary<string, string>();
            foreach (string file in filelist)
            {
                var filename = Path.GetFileNameWithoutExtension(file);
                rval.Add(filename, System.IO.File.ReadAllText(@file));
            }
            return rval;
        }
        private static List<string> GetFileList(string path, string filter, SearchOption option)
        {
            try
            {
                return System.IO.Directory.GetFiles(path, filter, option).ToList();
            }
            catch (Exception e)
            {
                throw new DirectoryNotFoundException($"An error occurred: {e}");
            }
            
        }
    }
}