#!/bin/bash

XML=`which xmlstarlet`
if ! [ 0 = $? ]; then
    exit 1;
fi
versionpre=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionPrefix" "../shoppinglist-update-webservice.csproj")
versionsuf=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionSuffix" "../shoppinglist-update-webservice.csproj")
repo=$(xmlstarlet sel -t -v "Project/PropertyGroup/RepositoryUrl" "../shoppinglist-update-webservice.csproj")

DIR=`pwd`

TMPDIR=`mktemp -d`

mkdir $TMPDIR/sluws-$versionpre

git clone --branch $versionpre-$versionsuf --depth 1 $repo $TMPDIR/sluws-$versionpre

if ! [ 0 = $? ]; then
    rm -rf $TMPDIR
    exit $?
fi

rm -rf "$TMPDIR/sluws-$versionpre/.git/"
rm -rf "$TMPDIR/sluws-$versionpre/packaging/"
rm -f "$TMPDIR/sluws-$versionpre/.gitlab-ci.yml"

cp -r $TMPDIR/sluws-$versionpre/* "../pkg/BUILD/"

cd $TMPDIR

tar --format=gnu -czf "$DIR/../pkg/SOURCES/sluws-$versionpre-$versionsuf-src.tar.gz" sluws-$versionpre

if ! [ 0 = $? ]; then
    rm -rf $TMPDIR
    exit $?
fi

# Cleanup

rm -rf $TMPDIR

exit 0