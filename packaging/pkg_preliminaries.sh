#!/bin/bash

if [ -d "../pkg" ]; then
    rm -rf "../pkg"
fi

mkdir -p "../pkg/BUILD"
mkdir -p "../pkg/BUILDROOT"
mkdir -p "../pkg/RPMS"
mkdir -p "../pkg/SOURCES"
mkdir -p "../pkg/SPECS"
mkdir -p "../pkg/SRPMS"

XML=`which xmlstarlet`
if ! [ 0 = $? ]; then
    exit $?;
fi

versionpre=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionPrefix" "../shoppinglist-update-webservice.csproj")
versionsuf=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionSuffix" "../shoppinglist-update-webservice.csproj")

sed "s/%%VERSIONPRE%%/$versionpre/g" < _template.spec > ../pkg/SPECS/sluws-$versionpre.spec
sed -i "s/%%VERSIONSUF%%/$versionsuf/g" ../pkg/SPECS/sluws-$versionpre.spec

exit 0