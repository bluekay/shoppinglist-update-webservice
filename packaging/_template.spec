#
# spec file for package sluws-%%VERSIONPRE%%-%%VERSIONSUF%%
#
# Copyright (c) 2019 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           sluws
Version:        %%VERSIONPRE%%
Release:        %%VERSIONSUF%%
Summary:        The webservice for the Shopping-List webapp
Vendor:         bluekay
License:        GPL-3.0-only
Group:          System Environment/Daemons
BuildArch:      noarch
Url:            https://gitlab.com/bluekay/shoppinglist-update-webservice
Source:         sluws-%%VERSIONPRE%%-%%VERSIONSUF%%-src.tar.gz
AutoReq:        no
BuildRequires:  dotnet-sdk-2.2
Requires:       dotnet-runtime-2.2
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
the webservice for the Shopping-List webapp

%prep
%setup -q
mkdir -p $RPM_BUILD_ROOT

%build
dotnet restore

%install
mkdir -p $RPM_BUILD_ROOT/opt/sluws
dotnet publish --configuration Release --self-contained --runtime opensuse-x64 --output $RPM_BUILD_ROOT/opt/sluws

%post
%postun

%files
%defattr(-,root,root)
/opt/sluws


%changelog

