#!/bin/bash

versionpre=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionPrefix" "../shoppinglist-update-webservice.csproj")
versionsuf=$(xmlstarlet sel -t -v "Project/PropertyGroup/VersionSuffix" "../shoppinglist-update-webservice.csproj")

cd "../pkg"

DIR=`pwd`

rpmbuild --define "_topdir $DIR" -ba SPECS/sluws-$versionpre.spec

exit $?;