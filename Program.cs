﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using static ShoppingUpdate.ConfigHandler;

namespace ShoppingUpdate
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls("http://0.0.0.0:5000")
                .UseStartup<Startup>()
                .Build();

            host.Run();
            
        }
    }

    public class Startup
    {
        private const string headerstring = "SLUpdateRequest";


        public void Configure(IApplicationBuilder app)
        {
            // Sample Config for intercepted calls BEFORE the app.Run()
            //app.Use(async (context, next) =>
            //{
            //    await next();
            //});
            Console.WriteLine("Running service version: " + System.Reflection.Assembly.GetEntryAssembly().GetName().Version);

            app.Run(async (context) =>
            {
                if (context.Request.Headers.ContainsKey(headerstring) && context.Request.Headers[headerstring] == "yes")
                {
                    Dictionary<string, string> versions = ConfigHandler.GetResources("/var/opt/sl_data");
                    
                    using (var bodyReader = new StreamReader(context.Request.Body))
		            {
                        var bodyAsText = bodyReader.ReadToEnd();
                        if (!string.IsNullOrWhiteSpace(bodyAsText))
                        {
                            if (versions.ContainsKey(bodyAsText))
                            {
                                context.Response.Headers.Add("Content-type", "application/json");
                                await context.Response.WriteAsync(versions[bodyAsText]);
                            }
                            else
                            {
                                context.Response.StatusCode = 404;
                                await context.Response.WriteAsync("404 Not Found");
                            }
                        }
                        else
                        {
                            context.Response.StatusCode = 404;
                            await context.Response.WriteAsync("404 Not Found");
                        }
                    }
                }
                else
                {
                    context.Response.StatusCode = 404;
                    await context.Response.WriteAsync("404 Not Found");
                }
                

                /*
                foreach (var thing in context.Request.Headers)
                {
                    Console.WriteLine(thing);
                }
                
                Headers:
                    context.Request.Headers : key-Value-Pair
                GET-Parameter:
                
                foreach (var key in context.Request.Query.Keys)
                {   
                    one parameter given via GET
                        key
                    value of that parameter, string.Empty if not set
                        var value;
                        context.Request.Query.TryGetValue(key, out var value);
                    
                    var msg = "DING: " + key + ", WERT: " + value;
                    Console.WriteLine(msg);
                }
                
                await context.Response.WriteAsync(context.Request.Query.Keys.ToString());
                */
            });
        }
    }
}
