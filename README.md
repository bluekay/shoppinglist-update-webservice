# ShoppingList-Update-WebService
| Branch | Status |
|:--:|:--:|
| **master** |   [![pipeline status](https://gitlab.com/bluekay/shoppinglist-update-webservice/badges/master/pipeline.svg)](https://gitlab.com/bluekay/shoppinglist-update-webservice/commits/master)   |
| **dev** | [![pipeline status](https://gitlab.com/bluekay/shoppinglist-update-webservice/badges/dev/pipeline.svg)](https://gitlab.com/bluekay/shoppinglist-update-webservice/commits/dev)|

---

# Update WebService
This project is intended to provide our shopping list webapp with a update-api backend. We intend to build it with .NET Core.


## systemd service
```
[Unit]
Description=The ShoppingList Updater WebService
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/path/to/dotnet /path/to/ShoppingList-Update-WebService.dll
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
